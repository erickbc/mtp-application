//
//  AppDelegate.swift
//  MTP
//
//  Created by Erick on 6/28/17.
//  Copyright © 2017 Erick. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import GoogleMaps
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    //User to get image and information
    var userLogin : User=User()
    
    let googleMapsApiKey = "AIzaSyCoPwNpNmrKfZTFRqFwn_DfSkNRLOyaERU"
    let locationManager = CLLocationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
         GMSServices.provideAPIKey(googleMapsApiKey)
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            
            if granted {
                UIApplication.shared.registerForRemoteNotifications()
            }
            
        }
        
         FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        
        return true
    }
    
    
    func handleEvent(forRegion region: CLRegion!) {
        
        
        // Show an alert if application is active
        if UIApplication.shared.applicationState != .active {
            // Otherwise present a local notification
            /*
             let content = UNMutableNotificationContent()
             content.title = "Demo Notification"
             content.subtitle = "Liverpool"
             content.body = "Esta oferta es para ti"
             content.categoryIdentifier = "message"
             let trigger = UNTimeIntervalNotificationTrigger(
             timeInterval: 10.0,
             repeats: false)
             let request = UNNotificationRequest(
             identifier: "10.second.message",
             content: content,
             trigger: trigger
             )
             UNUserNotificationCenter.current().add(
             request, withCompletionHandler: nil)
             */
            let identifier = ProcessInfo.processInfo.globallyUniqueString
            let content = UNMutableNotificationContent()
            content.title = region.identifier
            content.body = "Prueba"
            content.sound = UNNotificationSound.default()
            
            if (region.identifier == "Casa Familia Serrano"){
                if let attachment = UNNotificationAttachment.create(identifier: identifier, image: UIImage(named:"Complete.jpeg")!, options: nil) {
                    // where myImage is any UIImage that follows the
                    content.body = "Cerveza Gratis"
                    content.attachments = [attachment]
                }
            }else{
                if let attachment = UNNotificationAttachment.create(identifier: identifier, image: UIImage(named:"shop.png")!, options: nil) {
                    // where myImage is any UIImage that follows the
                    content.attachments = [attachment]
                }
            }

            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
            let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { (error) in
                // handle error
            }
            
       }else{
            
            if (region.identifier == "Casa Familia Serrano"){
            let alertMessage = UIAlertController(title: "Oferta Serrano's House", message: "Cerveza Gratis: \(region.identifier)", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: -20, y: -20, width: 70, height: 70))
                imageView.image =  UIImage(named: "002.png")
            
            
            
            //let image = UIImage(named: "star.png")
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            
            
            alertMessage.addAction(action)

           // action.setValue(image, forKey: "image")
           alertMessage.view.addSubview(imageView)
            
            window?.rootViewController?.present(alertMessage, animated: true, completion: nil)
            
            }
            else{
                let alertMessage = UIAlertController(title: "Oferta Disponible", message: "Existe una oferta que te puede interesar en: \(region.identifier)", preferredStyle: .alert)
                let imageView = UIImageView(frame: CGRect(x: -20, y: -20, width: 70, height: 70))
                imageView.image =  UIImage(named: "shopping-bag-flat.png")
                
                
                
                //let image = UIImage(named: "star.png")
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                
                
                
                alertMessage.addAction(action)
                
                // action.setValue(image, forKey: "image")
                alertMessage.view.addSubview(imageView)
                
                window?.rootViewController?.present(alertMessage, animated: true, completion: nil)
                

            }
           // window?.rootViewController?.showAlert(withTitle: "Existe una oferta que te puede interesar en: ", message:  region.identifier)

        }
    }
    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String! , annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        // Add any custom logic here.
        return handled;
        
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "MTP")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}




extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("Entry to te region")
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        print("Exit to te region")
        
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
}


