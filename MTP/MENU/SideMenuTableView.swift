//
//  SideMenuTableView.swift
//  SideMenu
//
//  Created by Jon Kent on 4/5/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import SideMenu

import FBSDKCoreKit
import FBSDKLoginKit


class SideMenuTableView: UITableViewController {
    
    @IBOutlet weak var viewProfileMenu: UIView!
    var userLogin:User? = nil
    @IBOutlet weak var ImageProfile: UIImageView!
    
    
    override func viewDidLoad() {
        
        
//        let loginManager = FBSDKLoginManager()
//        loginManager.logOut()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        userLogin = appDelegate.userLogin
        ImageProfile.image = userLogin?.image
        
        
        self.ImageProfile.layer.cornerRadius = self.ImageProfile.frame.size.width / 2;
        self.ImageProfile.layer.borderWidth = 6.0;
        
        let swiftColor = UIColor.getCustomColorLightOrange()
        self.ImageProfile.layer.borderColor = swiftColor.cgColor
        self.ImageProfile.clipsToBounds = true
        
    
        viewProfileMenu.backgroundColor = UIColor.clear
        
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("SideMenu Appearing!")
        
        // this will be non-nil if a blur effect is applied
        guard tableView.backgroundView == nil else {
            return
        }
        
        // Set up a cool background image for demo purposes
        let imageView = UIImageView(image: UIImage(named: "saturn"))
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        tableView.backgroundView = imageView
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("SideMenu Appeared!")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print("SideMenu Disappearing!")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        print("SideMenu Disappeared!")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 4){
            
            if (FBSDKAccessToken.current() != nil){
                print ("Aun Logeado")
            }else{
                 print ("Ya deslogeado")
            }
            
            let fm = FBSDKLoginManager()
                fm.logOut()
            
            
            if (FBSDKAccessToken.current() != nil){
                print ("Aun Logeado")
            }else{
                print ("Ya deslogeado")
            }
            
            
             performSegue(withIdentifier: "logout", sender: nil)
            
            
            
        }
    
    }
    
    
    
}
