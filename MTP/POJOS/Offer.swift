//
//  User.swift
//  ShareInApp
//
//  Created by Erick on 04/08/15.
//  Copyright (c) 2015 ShareIn. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

enum BackendError: Error {
    case urlError(reason: String)
    case objectSerialization(reason: String)
}



enum loyality:String {
    case Silver = "♠"
    case Gold = "♥"
    case Basic = "♦"
}


class OfferWrapper {
    var news: [Offer]?
    var count: Int?
}




class Offer {
    var id:String=""
    var priceLow:Float = 0.0
    var priceNormal:Float = 0.0
    var name:String = ""
    var description:String = ""
    var loyality:String = "Basic"
    var type:String = "Normal"
    var image:UIImage=UIImage()
    
    
    init(){
    }
    
    
    init(json: JSON){
        //print("JSON CONTRUCTOR")
        //print(json)
        //print ("JSON NAME OFFER")
        //print(json["pof_id"]["prd_id"][0]["name"])
        self.id=json["_id"].string!
        self.description=(json["pof_id"]["prd_id"][0]["name"]).string!
        self.name=(json["pof_id"]["oft_id"][0]["description"]).string!
        
        
        //let testArray:[JSON] = (json["img_id"]["img"]["data"]["data"]).array!
        
        if let testArray = (json["img_id"]["img"]["data"]["data"]).array{
        
            print("Xevere")
            
            let formattedArray:String = (testArray.map{String(describing: $0)}).joined(separator: ",")
            let array = formattedArray.components(separatedBy: ",")
            let intArray = array.map { UInt8($0)!}
            let datos: NSData = NSData(bytes: intArray, length: intArray.count)
            
            self.image = UIImage(data: datos as Data)!
            
            
        }
       
        
    }
    
    fileprivate class func getOfferAtPath(shop: Shop, completionHandler: @escaping (Result<OfferWrapper>) -> Void) {
        // make sure it's HTTPS because sometimes the API gives us HTTP URLs
    
            
        
       
            let _ = Alamofire.request(ServiceWeb.Router.getOfferDetail(shop.id)).responseJSON { response in
                
                if let error = response.result.error {
                    completionHandler(.failure(error))
                    return
                }
                //print (response)
                let newsWrapperResult = Offer.offersArrayFromResponse(response)
                completionHandler(newsWrapperResult)
            }
            
        
    }
    
    
    private class func offersArrayFromResponse(_ response: DataResponse<Any>) -> Result<OfferWrapper> {
        guard response.result.error == nil else {
            // got an error in getting the data, need to handle it
            //print(response.result.error!)
            return .failure(response.result.error!)
        }
        
        // make sure we got JSON and it's a dictionary
        //print (response.result.value)
        guard let json = response.result.value else {
            //print("didn't get species object as JSON from API")
            return .failure(BackendError.objectSerialization(reason:
                "Did not get JSON dictionary in response"))
        }
        //print ("iMPRIMIENDO json")
        //print (json)
        let wrapper:OfferWrapper = OfferWrapper()
//        wrapper.next = json["next"] as? String
//        wrapper.previous = json["previous"] as? String
//        wrapper.count = json["count"] as? Int
//        
        var allNew: [Offer] = []
        if let results = json as? [[String: Any]] {
            for jsonOffer in results {
                // Need to change
                let offer = Offer(json: JSON(jsonOffer))
                allNew.append(offer)
            }
        }
        wrapper.news = allNew
        return .success(wrapper)
    }
    
    
    class func getOffers(_shop:Shop,_ completionHandler: @escaping (Result<OfferWrapper>) -> Void) {
        getOfferAtPath(shop: _shop, completionHandler: completionHandler)
    }

    
    
}
