//
//  User.swift
//  ShareInApp
//
//  Created by Erick on 04/08/15.
//  Copyright (c) 2015 ShareIn. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class User:NSObject{
    var username:String=""
    var email:String=""
    var password:String=""
    var token:String=""
    var date_birth:String=""
    var id:String=""
    var idPush:String=""
    var aboutMe:String=""
    var origin_place:String=""
    var image:UIImage=UIImage()
    var mutualFriends:Int=0
    var totalTravels:Int=1
    var social_id:String=""
    var years:Int=1
    var year_Join:Int=1
    var month_join:Int=1
    
    
    override init(){}
    
    init(username:String,email:String,image:UIImage){
        self.username = username
        self.email = email
        self.image = image
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let username = aDecoder.decodeObject(forKey: "username") as! String
        let email = aDecoder.decodeObject(forKey: "email") as! String
        let image = aDecoder.decodeObject(forKey: "image") as! UIImage
        self.init(username: username , email: email, image: image)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(username, forKey: "username")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(image, forKey: "image")
    }

    
    
    init (username:String,id:String){
        self.id=id
        self.username=username
        // self.car=Car(id: idC, marca: marca, modelo: modelo)
    }
    
    
    init(JSON_User_Info jsonDictionary:JSON){
        self.id=jsonDictionary["id"].string!
        self.username=jsonDictionary["name"].string!
        self.email=jsonDictionary["email"].string!
        self.token=jsonDictionary["token"].string!
        
        if (jsonDictionary["placeOrigin"].null != nil){
            self.origin_place=jsonDictionary["placeOrigin"].string!
        }
        let imageD=jsonDictionary["photo"].string!
        let decodedData = Data(base64Encoded: imageD, options: NSData.Base64DecodingOptions(rawValue: 0))
        let decodedimage = UIImage(data: decodedData!)
        self.image = decodedimage!
    }
}
