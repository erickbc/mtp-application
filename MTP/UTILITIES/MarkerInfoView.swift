import UIKit

class MarkerInfoView: UIView {
    
    @IBOutlet weak var placePhoto: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        self.placePhoto.layer.cornerRadius = self.placePhoto.frame.size.width / 2;
        self.placePhoto.layer.borderWidth = 4.0;
        
        let swiftColor = UIColor.getCustomColorLStrongGray()
        
        
        self.placePhoto.layer.borderColor = swiftColor.cgColor
        self.placePhoto.clipsToBounds = true
            
       self.backgroundColor = UIColor(patternImage: UIImage(named: "Globe.png")!)
    }
}
