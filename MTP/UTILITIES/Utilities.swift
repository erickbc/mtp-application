/**
 * Copyright (c) 2016 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import MapKit
import UserNotifications


// MARK: Helper Extensions

extension CLRegion{
    private struct RegionExtraProperties{
        static var iconInfo:String? = nil
    }
    
    
    var iconInfo:String? {
    
        get{
            return objc_getAssociatedObject(self, &RegionExtraProperties.iconInfo) as? String
        }
        set{
            if let unwrappedValue = newValue{
                objc_setAssociatedObject(self, &RegionExtraProperties.iconInfo, unwrappedValue as NSString?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}

extension ZJLocationService {
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        currentViewController()?.present(alert, animated: true, completion: nil)
    }
}


extension UIViewController {
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}


extension MKMapView {
    func zoomToUserLocation() {
        guard let coordinate = userLocation.location?.coordinate else { return }
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 10000, 10000)
        setRegion(region, animated: true)
    }
}



extension UNNotificationAttachment {
    
    static func create(identifier: String, image: UIImage, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let tmpSubFolderName = ProcessInfo.processInfo.globallyUniqueString
        let tmpSubFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tmpSubFolderName, isDirectory: true)
        do {
            try fileManager.createDirectory(at: tmpSubFolderURL, withIntermediateDirectories: true, attributes: nil)
            let imageFileIdentifier = identifier+".png"
            let fileURL = tmpSubFolderURL.appendingPathComponent(imageFileIdentifier)
            guard let imageData = UIImagePNGRepresentation(image) else {
                return nil
            }
            try imageData.write(to: fileURL)
            let imageAttachment = try UNNotificationAttachment.init(identifier: imageFileIdentifier, url: fileURL, options: options)
            return imageAttachment
        } catch {
            print("error " + error.localizedDescription)
        }
        return nil
    }
}


extension UIImage {
    class func imageWithView(view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

extension MapView{
    
    func handledNotificationSerranoHouse(){
    
        if UIApplication.shared.applicationState != .active {
            // Otherwise present a local notification
            /*
             let content = UNMutableNotificationContent()
             content.title = "Demo Notification"
             content.subtitle = "Liverpool"
             content.body = "Esta oferta es para ti"
             content.categoryIdentifier = "message"
             let trigger = UNTimeIntervalNotificationTrigger(
             timeInterval: 10.0,
             repeats: false)
             let request = UNNotificationRequest(
             identifier: "10.second.message",
             content: content,
             trigger: trigger
             )
             UNUserNotificationCenter.current().add(
             request, withCompletionHandler: nil)
             */
            let identifier = ProcessInfo.processInfo.globallyUniqueString
            let content = UNMutableNotificationContent()
            content.sound = UNNotificationSound.default()
            content.title = "Oferta Serrano's House"
            content.body = "Promocion Cerveza Gratis"
            if let attachment = UNNotificationAttachment.create(identifier: identifier, image: UIImage(named:"002.jpg")!, options: nil) {
                // where myImage is any UIImage that follows the
                content.attachments = [attachment]
            }
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
            let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { (error) in
                // handle error
            }
            
        }else{
            
            let alertMessage = UIAlertController(title: "Oferta Serrano's House", message: "Promocion Cerveza Gratis", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: -20, y: -20, width: 70, height: 70))
            imageView.image =  UIImage(named: "002.jpg")
            //let image = UIImage(named: "star.png")
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertMessage.addAction(action)
            
            // action.setValue(image, forKey: "image")
            alertMessage.view.addSubview(imageView)
            
            self.present(alertMessage, animated: true, completion: nil)
            
            //window?.rootViewController?.present(alertMessage, animated: true, completion: nil)
            
            
            // window?.rootViewController?.showAlert(withTitle: "Existe una oferta que te puede interesar en: ", message:  region.identifier)
            
        }
    
    }

    func handledNotification() {
        
        
        // Show an alert if application is active
        if UIApplication.shared.applicationState != .active {
            // Otherwise present a local notification
            /*
             let content = UNMutableNotificationContent()
             content.title = "Demo Notification"
             content.subtitle = "Liverpool"
             content.body = "Esta oferta es para ti"
             content.categoryIdentifier = "message"
             let trigger = UNTimeIntervalNotificationTrigger(
             timeInterval: 10.0,
             repeats: false)
             let request = UNNotificationRequest(
             identifier: "10.second.message",
             content: content,
             trigger: trigger
             )
             UNUserNotificationCenter.current().add(
             request, withCompletionHandler: nil)
             */
            let identifier = ProcessInfo.processInfo.globallyUniqueString
            let content = UNMutableNotificationContent()
            content.sound = UNNotificationSound.default()
            content.title = "Ofertas Cercanas a ti"
            content.body = "Existen algunas ofertas cercanas a ti que te pueden interesar..."
            if let attachment = UNNotificationAttachment.create(identifier: identifier, image: UIImage(named:"shop.png")!, options: nil) {
                // where myImage is any UIImage that follows the
                content.attachments = [attachment]
            }
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
            let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { (error) in
                // handle error
            }
            
        }else{
            
            let alertMessage = UIAlertController(title: "Oferta Disponible", message: "Existen algunas ofertas que te puede interesar...", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: -20, y: -20, width: 70, height: 70))
            imageView.image =  UIImage(named: "shopping-bag-flat.png")
            
            
            
            //let image = UIImage(named: "star.png")
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            
            
            alertMessage.addAction(action)
            
            // action.setValue(image, forKey: "image")
            alertMessage.view.addSubview(imageView)
            
            self.present(alertMessage, animated: true, completion: nil)
            
            //window?.rootViewController?.present(alertMessage, animated: true, completion: nil)
            
            
            // window?.rootViewController?.showAlert(withTitle: "Existe una oferta que te puede interesar en: ", message:  region.identifier)
            
        }
    }

    
    

}


