//
//  CommentViewController.swift
//  ShareInApp
//
//  Created by Erick on 9/24/15.
//  Copyright (c) 2015 ShareIn. All rights reserved.
//

import UIKit
import Alamofire

class BuyView: UIViewController, UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate{
    //--------Outlets-------------------------
    @IBOutlet weak var commentText: UITextView!
    @IBOutlet weak var contenedorImage: UIView!
    @IBOutlet weak var buttonCancel: UIButton!
    
    @IBOutlet weak var imageSelect: UIImageView!
    
    var imageSelecteYet:Bool = false
    @IBOutlet weak var widthImage: NSLayoutConstraint!
    @IBOutlet weak var alturaConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var spaceBetwenPC: NSLayoutConstraint!
    
    @IBOutlet weak var scrollContent: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var progressSimbol: UIActivityIndicatorView!
    
    @IBOutlet weak var buttonSendBuy: UIButton!
    
    let homeButton:UIButton = UIButton()
    let selectPhoto:UIButton = UIButton()
    let AddButton:UIButton = UIButton()
    
    //----------------------------------------
    
    //..............VARS......................
    var bandera=false
    var photoSelect=false
    var picker:UIImagePickerController?=UIImagePickerController()
    //var popover:UIPopoverController?=nil

    
    @IBAction func sendBuy(_ sender: Any) {
        
        performSegue(withIdentifier: "returnMap", sender: nil)

    }
    
    //........................................
    
    //-----------------ACTIONS----------------
    

    //----------------------------------------
    
    //...............METHODS..................




    func imageResize (_ imageObj:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        imageObj.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    

    func textViewDidEndEditing(_ textView: UITextView) {
        if commentText.text.isEmpty {
            commentText.text = "Write here your comment"
            commentText.textColor = UIColor.lightGray
        }
    }
    //........................................
    
    //----------------VIEWS---------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonSendBuy.isHidden = true
        //widthImage.constant=0
               picker?.delegate=self
        
          }
    
    //------------------------------------------
    
    //.................GENERAL FUNCTIONS..........................

    //...........................................................
    
    //..............FUNCS CAMERA .....................
    
    @IBAction func camaraPicker(_ sender: Any) {
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
            
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            
        }
        
        // Add the actions
        picker?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func btnImagePickerClicked(_ sender: AnyObject)
    {
        //print("Seleccionando")
        
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            self .present(picker!, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let imS = info[UIImagePickerControllerOriginalImage] as! UIImage
        //imageView.contentMode = .ScaleAspectFit
        //imageView.image = chosenImage
        dismiss(animated: true, completion: nil)
        
        
        
        
        
        imageSelect.image = imS
        
        imageSelecteYet = true
        
         buttonSendBuy.isHidden = false
        //buttonCancel.isHidden = false
        //photoSelect=true
        //widthImage.constant=240
        //alturaConstraint.constant = 0
        //spaceBetwenPC.constant = 30
        
        //contenedorImage.backgroundColor=UIColor(patternImage: imageObbj)
        
        //bandera = false
        
        
        
        
        
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker .dismiss(animated: true, completion: nil)
    }
    //................................................
    
    
    
    
}
