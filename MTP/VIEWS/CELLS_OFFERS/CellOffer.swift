//
//  HomeBestStyleCommentCell.swift
//  ShareInApp
//
//  Created by Erick on 9/21/15.
//  Copyright (c) 2015 ShareIn. All rights reserved.
//

import UIKit

class CellOffer: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var nameOffer: UILabel!
    @IBOutlet weak var imageOffer: UIImageView!
    @IBOutlet weak var descriptionOffer: UILabel!
    @IBOutlet weak var priceOffer: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
            print("Width")
        
            print(self.frame.width)
        
        
       // let whiteRoundedCornerView:UIView = UIView(frame: CGRect(x: 5,y: 5,width: 365,height: 135))
        
        viewBackground.backgroundColor = UIColor.white
        viewBackground.layer.masksToBounds = false
        viewBackground.layer.cornerRadius = 3.0;
        viewBackground.layer.shadowOffset = CGSize(width: -1, height: 1);
        viewBackground.layer.shadowOpacity = 0.5
        
        
        self.backgroundColor = UIColor.clear
        
        
        //self.contentView.addSubview(whiteRoundedCornerView)
       // self.contentView.sendSubview(toBack: whiteRoundedCornerView)
        
        //----
        
        self.imageOffer.layer.cornerRadius = self.imageOffer.frame.size.width / 2;
        self.imageOffer.layer.borderWidth = 4.0;
        
        let swiftColor = UIColor.getCustomColorLLightGray()
        
        
        self.imageOffer.layer.borderColor = swiftColor.cgColor
        self.imageOffer.clipsToBounds = true
        
        
        
    
    }
    
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
   
}
