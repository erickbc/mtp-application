import UIKit
import GoogleMaps
import RNCryptor
import PulsingHalo
import Alamofire
import SwiftyJSON
import CoreLocation

import SideMenu


class MapView: UIViewController,GMSMapViewDelegate {
    
    
    var blockThread:Bool = false
    var timer = Timer()
    
    var userLogin:User? = nil
    
    
    var menuTransitionManager = MenuTransitionManager()
    @IBOutlet var mapaView: GMSMapView!
    var infoMarker:String = ""
    
    //----------Core Location-------------------
    
    var locationManager: CLLocationManager?
    var monitoringLocation: CLLocation?
    
    var markerLocation  = GMSMarker()
    
    
    var status:Status = Status()
    
    var lastLocation:CLLocation?
    
    //------------------------------------------
    
    
    func custonLabels(){
//        if let navigationBar = self.navigationController?.navigationBar {
//            let firstFrame = CGRect(x: navigationBar.frame.width/2, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
//           
//            
//            let firstLabel = UILabel(frame: firstFrame)
//            firstLabel.text = "Me siento a poca madre"
//            
//           
//            
//            navigationBar.addSubview(firstLabel)
//        }
        
//        SearchButton.frame =  CGRect(x:100.0, y: 5.0, width: 25.0, height: 25.0)
//        SearchButton.setImage(UIImage(named: "icon_search"), for: UIControlState.normal)
//        SearchButton.addTarget(self, action: Selector(("serachTravel:")), for: UIControlEvents.touchUpInside)
        
        
    //self.navigationItem.title = "Hoy estoy a pocoa madre"
    }
    
    func timerAction() {
        if (!blockThread){
            
            let date = Date()
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date)
            let minutes = calendar.component(.minute, from: date)

            
            
            print("SomeCoolTaskRunning.....\(hour) - \(minutes)")
            configureMap(location: lastLocation,background:true)
            //self.handledNotification()
           
            
        }else{
            print("Thread Block")
        }
    }
    
    
    
    @IBAction func seeStatus(_ sender: UIBarButtonItem) {
        
        performSegue(withIdentifier: "seeStatus", sender: nil)
        
        
    }
    override func viewDidLoad() {
        
        //---
        
        
        timer.invalidate()
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        timer = Timer.scheduledTimer(timeInterval: 180, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
        //---
        
        super.viewDidLoad()
        setupSideMenu()
        custonLabels()
        //Charge information user
        
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
            userLogin = appDelegate.userLogin
        
        
        let defaults = UserDefaults.standard
            defaults.object(forKey: "Image")
        
        let yourImage = UIImage(data:defaults.object(forKey: "Image") as! Data)
            userLogin?.image = yourImage!
        
        
        //------------Configuration GEOLOCALIZATION-------------------
        
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.delegate = self
        monitoringLocation = CLLocation(latitude: 37.331600000000002, longitude: -122.0301)
        locationManager?.requestAlwaysAuthorization()
        locationManager?.distanceFilter = 100
        
        //-------------------Configuration --------------------------
        mapaView.delegate = self
        
        
        
        let camera = GMSCameraPosition.camera(withLatitude: -33.868,
                                              longitude:151.2086, zoom:20)
        
        
        mapaView.camera = camera
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        
        
        let rect =  CGRect(x: 0, y: (self.view.frame.height * 0.12), width: 155, height: 60)

        
        
      let customInfoWindow = Bundle.main.loadNibNamed("Weather", owner: self, options: nil)?[0] as! Weather
        customInfoWindow.frame = rect
        customInfoWindow.backgroundColor = UIColor.clear
        
        
        //---Create Logo in Map 
        
        let imageName = "7BBB334F-8B5D-42F6-84EB-4A0ADF22C03E.png"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        //Finally you'll need to give imageView a frame and add it your view for it to be visible:
        
        imageView.frame = CGRect(x: self.view.bounds.maxX - 145, y:  self.view.bounds.maxY - 70, width: 150, height: 105)
        
        
        self.view.addSubview(imageView)
        
        self.view.addSubview(customInfoWindow)
        
    }
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // Set up a cool background image for demo purposes
        SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "back_xhdpi_perfil.png")!)
    }

    /*
    
    @IBAction func updateLocation(_ sender: UIBarButtonItem) {
        
        mapaView.clear()
        
        
        let path = GMSMutablePath()
        
        let locationLiverpool = CLLocationCoordinate2DMake(CLLocationDegrees(19.433627419907445), CLLocationDegrees(-99.18267667293549))
        let locationBoston = CLLocationCoordinate2DMake(CLLocationDegrees(19.433417479821724),  CLLocationDegrees(-99.18586984276772))
        let locationInnova = CLLocationCoordinate2DMake(CLLocationDegrees(19.4335123323037), CLLocationDegrees(-99.18448850512505))
        
        
        
        
        let markerLiverpool = GMSMarker(position: locationLiverpool)
        markerLiverpool.icon = GMSMarker.markerImage(with: UIColor.black)
        markerLiverpool.map = mapaView
        markerLiverpool.userData = "Liverpool"
        markerLiverpool.icon = UIImage(named:"favicon.png")
        path.add(locationLiverpool)
        
        
        let markerBoston = GMSMarker(position: locationBoston)
        markerBoston.icon = GMSMarker.markerImage(with:UIColor.blue)
        markerBoston.map = mapaView
        markerBoston.userData = "Boston"
        path.add(locationBoston)
        
        
        
        let markerInovva = GMSMarker(position: locationInnova)
        markerInovva.icon = GMSMarker.markerImage(with:UIColor.green)
        markerInovva.map = mapaView
        markerInovva.userData = "Innova"
        path.add(locationInnova)
        
        let camera = GMSCameraPosition.camera(withLatitude: locationBoston.latitude,
                                              longitude: locationBoston.longitude,
                                              zoom: 17)
        
        
        mapaView.camera = camera
        
        
        
        
        
        
        
    }
    
    

    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
       
        
        
        //let index:Int! = Int(marker.accessibilityLabel!)
        // 2
        let customInfoWindow = Bundle.main.loadNibNamed("MarkerInfoView", owner: self, options: nil)?[0] as! MarkerInfoView
        customInfoWindow.nameLabel.text = "Erick"
        
        
        
       
        let imageName = "favicon.png"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        
        
         customInfoWindow.placePhoto = imageView
        return customInfoWindow
        
        
        
//    
//        if let infoView = (UINib(nibName: "MarkerInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView) as? MarkerInfoView {
//            infoView.nameLabel.text = placeMarker.place.name
//            
//            if let photo = placeMarker.place.photo {
//                infoView.placePhoto.image = photo
//            } else {
//                infoView.placePhoto.image = UIImage(named: "generic")
//            }
//            
//            return infoView
//        } else {
//            return nil
//        }
    }
    */

    //We need to implement
//    func getOffers(latitud:CLLocationDegrees , longitud:CLLocationDegrees) -> [GMSMarker]{
//        var arrayMarkers : [GMSMarker] = []
//        DispatchQueue.global(qos: .default).async { // 1
//            Alamofire.request(ServiceWeb.Router.getNearStores(String(latitud), String(longitud))).responseJSON{ response in
//                
//                if response.result.error == nil{
//                    let info = JSON(response.result.value!)
//                   
//                    for (_,subJson):(String, JSON) in info {
//                        //Do something you want
//                       
//                        let shop = Shop(JSON_User_Info: subJson)
//                        
//                        let locationM = CLLocationCoordinate2DMake(CLLocationDegrees(shop.lat), CLLocationDegrees(shop.lon))
//                        
//                        let markerL = GMSMarker(position: locationM)
//                            markerL.icon = GMSMarker.markerImage(with: UIColor.black)
//                            markerL.userData = shop.name
//                            markerL.iconView = self.customViewMarkerShop(image: UIImage(named: "shop.png")!)
//                       
//                        arrayMarkers.append(markerL)
//
//                    }
//                }else{
//                    print("Error")
//                    print(response.result.error!)
//                }
//                
//                
//                DispatchQueue.main.async { // 2
//                    return arrayMarkers
//                }
//            }
//            
//        }
//    }
    
    
    
    func customizeMarker() -> UIView{
        
  
        let markerview:UIView = UIView(frame: CGRect(x: 0, y: 0, width: 55, height: 102))
           // markerview.backgroundColor = UIColor(patternImage: UIImage(named:"Globe.png")!)
             markerview.backgroundColor  = UIColor.clear
            markerview.isOpaque = false
        let imgView:UIImageView =  UIImageView(frame: CGRect(x: 4, y: 4, width: 47, height: 70))
        
            imgView.backgroundColor = UIColor.white
        imgView.contentMode = UIViewContentMode.scaleAspectFit
        imgView.image = UIImage(named:"Globe.png")!
        imgView.layer.cornerRadius = 7
        imgView.layer.masksToBounds = true
        markerview.addSubview(imgView)
        
        return markerview
        
    }
    


    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("Seleccionaste un markador")
       // infoMarker = marker.userData!
        //Extract info marker
        let shopSelected:Shop =  marker.userData! as! Shop
        if(shopSelected.name != "Local"){
            //performSegue(withIdentifier: "info", sender: nil)
            performSegue(withIdentifier: "seeOffers", sender: shopSelected)
            
        }
        return true
    }
    
 


}


//Animation to view Product
extension MapView: MenuTransitionManagerDelegate{
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "info"){
            self.menuTransitionManager.delegate = self
            let menuTableViewController = segue.destination as! InfoProduct
            menuTableViewController.info = infoMarker
            //menuTableViewController.
            menuTableViewController.transitioningDelegate = menuTransitionManager
            //menuTableViewController.currentItem = self.title!
        }else if(segue.identifier == "seeOffers"){
            let detailVC=segue.destination as! TableOffersDetail
                detailVC.store = sender as! Shop
        }else if (segue.identifier == "seeStatus"){
            
            self.menuTransitionManager.delegate = self
            let menuTableViewController = segue.destination as! ViewTableStatus
            //menuTableViewController.info = infoMarker
            //menuTableViewController.
            menuTableViewController.transitioningDelegate = menuTransitionManager

        
        
        }
     
        
    }
    
    @IBAction func unwindToHome(_ segue: UIStoryboardSegue) {
        print ("Regresando")
        //let sourceController = segue.source as! MapView
        // self.title = sourceController.currentItem
        
        
        if let sourceViewController = segue.source as? ViewTableStatus {
            status = sourceViewController.statusSelect
            
             self.navigationItem.title = "Hoy me siento \(status.description)"
        }
        
        
    }
    
    
    func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension MapView:CLLocationManagerDelegate{
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
       
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            
            if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
                
                locationManager?.startUpdatingLocation()
                locationManager?.allowsBackgroundLocationUpdates = true
                
              
                
                
            }
            
           
           
        }
    }
    
    func orderGeofances(){
        
        
//        for item in (self.locationManager?.monitoredRegions)! {
//            self.locationManager?.stopMonitoring(for: item)
//        }
        
    
    }
    
    
    func configureMap(location:CLLocation?,background:Bool) {
    
        var findLocations:Bool = false
        
        //if UIApplication.shared.applicationState == .active {
        print ("Configuracion Active")
        var arrayMarkers : [GMSMarker] = []
        markerLocation.position = CLLocationCoordinate2D(latitude: (location!.coordinate.latitude), longitude: (location!.coordinate.longitude))
        //Call WS
        
        
        //Update Location Manually
        var findSerranoHaouse:Bool = false
        
        //-------------------------
        let la:CLLocationDegrees = (location!.coordinate.latitude)
        let ln:CLLocationDegrees = (location!.coordinate.longitude)
        Alamofire.request(ServiceWeb.Router.getNearStores(String(la), String(ln))).responseJSON{ response in
            if response.result.error == nil{
                let info = JSON(response.result.value!)
                for (_,subJson):(String, JSON) in info {
                    //Make notification that demostrate that there are some shops
                    let shop = Shop(JSON_User_Info: subJson)
                    let locationM = CLLocationCoordinate2DMake(CLLocationDegrees(shop.lat), CLLocationDegrees(shop.lon))
                    let markerL = GMSMarker(position: locationM)
                    markerL.icon = GMSMarker.markerImage(with: UIColor.black)
                    markerL.userData = shop
                    markerL.iconView = self.customViewMarkerShop(image: UIImage(named: "shop.png")!)
                    arrayMarkers.append(markerL)
                    
                    findLocations=true
                    if(shop.name == "Casa Familia Serrano"){
                        findSerranoHaouse = true
                         markerL.iconView = self.customViewMarkerShop(image: UIImage(named: "002.jpg")!)
                    }
                    
                    //---Put in Monitor to check if the app will go to background
                    //if ((self.locationManager?.monitoredRegions.count)! < 20){
                    if ((self.locationManager?.monitoredRegions.count)! < 20){
                        let region = CLCircularRegion(center: (locationM), radius: 300, identifier: shop.name)
                        //region.setValue("shop.png", forKey: "icon")
                        region.notifyOnEntry = true
                        region.notifyOnExit = false
                        self.locationManager?.startMonitoring(for: region)
                    }else{
                        let limitToDelete = 20 - (self.locationManager?.monitoredRegions.count)!
                        for _ in stride(from: 0, to: limitToDelete-1, by: 1) {
                            let r = self.locationManager?.monitoredRegions.first
                            self.locationManager?.stopMonitoring(for: r!)
                        }
                    }
                    //-----------------------------------------------------------
                }
                
                if (background && findLocations){
                    if(findSerranoHaouse == false){
                        self.handledNotification()
                    }else{
                        self.handledNotificationSerranoHouse()
                    }
                }
                
                
                
                
                self.mapaView.clear()
                for m in arrayMarkers{
                    m.map = self.mapaView
                }
                self.markerLocation.icon = GMSMarker.markerImage(with:UIColor.green)
                self.markerLocation.map = self.mapaView
                
                let localShop:Shop = Shop()
                localShop.name = "Local"
                self.markerLocation.userData = localShop
                self.markerLocation.iconView = self.customViewMarkerLocal ();
                let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!,longitude: (location?.coordinate.longitude)!,zoom: 17)
                self.mapaView.camera = camera
                
                
               
            }else{
                print("Error")
                print(response.result.error!)
            }
            
            
        }
        
        //}

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        blockThread = true
        let location = locations.first
        
        lastLocation = location!
        
        configureMap(location: location,background: false)
        
        blockThread = false
        
    }
    
    
    
    func customViewMarkerLocal() -> MarkerInfoView{
        let image =  (userLogin?.image)!
        
        let customInfoWindow = Bundle.main.loadNibNamed("MarkerInfoView", owner: self, options: nil)?[0] as! MarkerInfoView
        customInfoWindow.nameLabel.text = ""
        
        customInfoWindow.placePhoto.image = image
        
        return customInfoWindow
        //markerLocation.iconView = customInfoWindow;
    
    }
    
    func customViewMarkerShop(image:UIImage) -> MarkerInfoView{
        
        let customInfoWindow = Bundle.main.loadNibNamed("MarkerInfoView", owner: self, options: nil)?[0] as! MarkerInfoView
        customInfoWindow.nameLabel.text = ""
        
        customInfoWindow.placePhoto.image = image
        
        return customInfoWindow
        //markerLocation.iconView = customInfoWindow;
        
    }
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        print("Region Enter:: \(region.identifier)")
        
        print("entered region")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Region Exit:: \(region.identifier)")
        print("exited region")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print(error.localizedDescription)
        print("error");
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("region error");
    }
    
    
    func stopMonitoring(geotification: Geotification) {
        for region in (locationManager?.monitoredRegions)! {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geotification.identifier else { continue }
            locationManager?.stopMonitoring(for: circularRegion)
        }
    }
    
    
    
    

}
