//
//  TableOffersDetail.swift
//  MTP
//
//  Created by Erick on 7/4/17.
//  Copyright © 2017 Erick. All rights reserved.
//

import UIKit


class TableOffersDetail: UITableViewController{
    
    //ELEMNTS NECESARY CALL WS
    
    var offers: [Offer]?
    var offersWrapper: OfferWrapper? // holds the last wrapper that we've loaded
    var isLoadingOffers = false
    
    
    
    var store:Shop = Shop()
    override func viewDidLoad() {
        print("Chare View Table")
        self.view.backgroundColor = UIColor.getCustomColorLLightGray()
        self.loadFirstOffers()
        

            
            //UIColor(patternImage: UIImage(named: "back_xhdpi_perfil.png")!)
    }
    
    func loadFirstOffers() {
        isLoadingOffers = true
        
        
        Offer.getOffers(_shop: store) { result in
            if let error = result.error {
                // TODO: improved error handling
                self.isLoadingOffers = false
                let alert = UIAlertController(title: "Error", message: "Could not load first species :( \(error.localizedDescription)", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            let offersWrapper = result.value
            self.addOffersFromWrapper(offersWrapper)
            self.isLoadingOffers = false
            self.tableView.reloadData()
        }
    }
    
    func addOffersFromWrapper(_ wrapper: OfferWrapper?) {
        self.offersWrapper = wrapper
        if self.offers == nil || self.offers?.count==0 {
          
            let r:Offer=Offer()
                r.type = "Header"
            
            self.offers = self.offersWrapper?.news
            
            self.offers?.insert(r, at: 0)
        } else if self.offersWrapper != nil && self.offersWrapper!.news != nil {
            self.offers = self.offers! + (self.offersWrapper?.news)!
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.offers == nil {
            return 0
        }
        return (self.offers?.count)!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
        
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "seeDescription", sender: nil)

    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        var cell:UITableViewCell
        
        
        if (indexPath.row == 0){
            let cellPrincipal = (tableView.dequeueReusableCell(withIdentifier: "CellShop", for: indexPath as IndexPath) ) as! CellShop
            cellPrincipal.selectionStyle = UITableViewCellSelectionStyle.none
            
            cellPrincipal.addressShop.text = "\(store.street) #\(store.number) C.P \(store.zip)"
             cellPrincipal.descriptionShop.text = "Tienda Departamental"
            cellPrincipal.shopName.text = store.name
            
            
            cell = cellPrincipal
            
            return cell
        }else{
            
            if self.offers != nil && self.offers!.count >= indexPath.row {
                let of = self.offers![indexPath.row]
                
                let cellPrincipal = (tableView.dequeueReusableCell(withIdentifier: "CellOffer", for: indexPath as IndexPath) ) as! CellOffer
            
                cellPrincipal.backgroundColor = UIColor.clear
            
                cellPrincipal.descriptionOffer.text = of.description
                cellPrincipal.nameOffer.text = of.name
                cellPrincipal.priceOffer.text = "$ 3789.00"
                cellPrincipal.imageOffer.image = of.image
           
                cell = cellPrincipal
            
                return cell
            }
        
        }
        return UITableViewCell()
    
    }
    

}
