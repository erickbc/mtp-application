//
//  ViewController.swift
//  MTP
//
//  Created by Erick on 6/28/17.
//  Copyright © 2017 Erick. All rights reserved.
//

import Alamofire
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


class ViewController: UIViewController,FBSDKLoginButtonDelegate{

    var botonClick=false
    @IBOutlet weak var loginFacebook: FBSDKLoginButton!
    
    //@IBOutlet weak var loading: UIActivityIndicatorView!
    var userLogin:User? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
         loginFacebook.readPermissions = ["public_profile", "email", "user_friends"]
        
        loginFacebook.delegate = self
        
        //Charge User
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
            userLogin = appDelegate.userLogin
        

//        if(!botonClick){
//            
//            if (FBSDKAccessToken.current() != nil){
//                //let tokenFa = FBSDKAccessToken.current().tokenString
//                
//                self.performSegue(withIdentifier: "showMap", sender: self)
//            }
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
         super.viewWillAppear(animated)
         self.navigationController?.isNavigationBarHidden = true;
        if(!botonClick){
            
            
            
            if (FBSDKAccessToken.current() != nil){
                //let tokenFa = FBSDKAccessToken.current().tokenString
                print ("lOGGEADO")
                self.performSegue(withIdentifier: "showMap", sender: self)
            }
        }
//
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false;
    }
    
    
    @IBAction func unwindToPrincipal(_ segue: UIStoryboardSegue) {
        print ("Roll Back")
       
        
        
    }
    
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        botonClick=true
        if error == nil{
            
            let tokenFa = FBSDKAccessToken.current().tokenString
            print (tokenFa)
            //DispatchQueue.main.async {
                Alamofire.request("https://graph.facebook.com/me/picture?type=large&return_ssl_resources=1&access_token="+tokenFa!).response{
                response in
                    let image = UIImage(data: response.data!)
                    self.userLogin?.image = image!
                    
                    
                    let imageData = UIImageJPEGRepresentation(image!, 1.0)
                    
                    
                    let defaults = UserDefaults.standard
                        defaults.set(imageData, forKey: "Image")
                    
                    
                    self.performSegue(withIdentifier: "showMap", sender: self)
                    
                    
                    
                }
               
            
            
        }else{
            print("Error")
            print(error)
            
        }

    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Salida")
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

