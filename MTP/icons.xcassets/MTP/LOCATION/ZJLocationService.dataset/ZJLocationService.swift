		//
//  LocationService.swift
//  ZJBatterySaveLocation
//
//  Created by ZeroJianMBP on 16/4/19.
//  Copyright © 2016年 ZeroJian. All rights reserved.
//

import CoreLocation
import UIKit


struct PreferencesKeys {
    static let savedItems = "savedItems"
}

class ZJLocationService: NSObject,CLLocationManagerDelegate {
  
  
    
  var geotifications: [Geotification] = []
    
    
  static let sharedManager = ZJLocationService()

  
  private var backgroundTask = BackgroundTask()
  private var timeInterval: Double = 179
  private var timer: Timer?
  
  class var time: TimeInterval! {
    get {
      return self.sharedManager.timeInterval
    }
    set(second) {
      if second > 0 && second < 180 {
        self.sharedManager.timeInterval = second
      }
    }
  }
  
  private var backgroundLocations = [CLLocation]() {
    didSet{
      if backgroundLocations.count == 10 {
        
        begionBackgroundTask(time: self.timeInterval)
      }
    }
  }
  
  var didUpdateLocation: ((CLLocation) -> Void)?
  
  class func startLocation() {
    if (CLLocationManager.locationServicesEnabled()) {
      self.sharedManager.locationManager.startUpdatingLocation()
      print("begin updating location")
    }
  }
  
  class func stopLocation() {
    self.sharedManager.locationManager.stopUpdatingLocation()
    print("did stop location")
  }
  
  lazy var locationManager: CLLocationManager = {
    let locationManager = CLLocationManager()
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    locationManager.pausesLocationUpdatesAutomatically = false
    locationManager.distanceFilter = kCLDistanceFilterNone
    if #available(iOS 9.0, *) {
      locationManager.allowsBackgroundLocationUpdates = true
    }
    if #available(iOS 8.0, *) {
    locationManager.requestAlwaysAuthorization()
    }
    return locationManager
  }()
    
    
    func startMonitoring(geotification: Geotification) {
        // 1
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            showAlert(withTitle:"Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        // 3
        let region = self.region(withGeotification: geotification)
        // 4
        locationManager.startMonitoring(for: region)
    }
    
    func region(withGeotification geotification: Geotification) -> CLCircularRegion {
        // 1
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
        // 2
        region.notifyOnEntry = (geotification.eventType == .onEntry)
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
  
    
    
    func saveAllGeotifications() {
        var items: [Data] = []
        for geotification in geotifications {
            let item = NSKeyedArchiver.archivedData(withRootObject: geotification)
            items.append(item)
        }
        UserDefaults.standard.set(items, forKey: PreferencesKeys.savedItems)
    }
    
    
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let newLocation = locations.last else { return }
    
    didUpdateLocation?(newLocation)
    
    if UIApplication.shared.applicationState != .active {
      print("background location : \(newLocation.coordinate.latitude), \(newLocation.coordinate.longitude)")
        
        
        //------
        
        let coordinate = newLocation.coordinate
        let radius = 100
        let identifier = "Location1"
        let note = "Location 1"
        let eventType: EventType = .onExit
        
        let clampedRadius = min(radius, Int(locationManager.maximumRegionMonitoringDistance))
        let geotification = Geotification(coordinate: coordinate, radius: CLLocationDistance(clampedRadius), identifier: identifier, note: note, eventType: eventType)
        add(geotification: geotification)
        // 2
        startMonitoring(geotification: geotification)
        saveAllGeotifications()
        
        
        
        if (geotifications.count < 20){
            backgroundLocations.append(newLocation)
        }
        //------
    } else {
      print("active status location:  \(newLocation.coordinate.latitude), \(newLocation.coordinate.longitude)")
      
      initialBackgroundTask()
      
      if backgroundLocations.count > 0 {
        backgroundLocations.removeAll()
      }
    }
  }
    
    //--------------------------------
    
    func add(geotification: Geotification) {
        geotifications.append(geotification)
        //mapView.addAnnotation(geotification)
        //addRadiusOverlay(forGeotification: geotification)
        //(()updateGeotificationsCount()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status{
        case .denied:
            showAlert(withTitle: "",message: "")
            print("Denegada")
        default:
            break
        }
    }
  
//  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
//    switch status {
//    case .denied:
//      showAlert()
//      print("AuthorizationStatus -- Denied")
//    default:
//      break
//    }
//  }
  
//  private func showAlert() {
//    if #available(iOS 8.0, *) {
//      let alertController = UIAlertController(title: "Confirm", message: "This App does not have access to Location Services,Please enable in Settings", preferredStyle: .alert)
//      let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
//      alertController.addAction(action)
//      
//     currentViewController()?.present(alertController, animated: true, completion: nil)
//
//    } else {
//      
//      let alertView = UIAlertView(title: "Confirm", message: "This App does not have access to Location Services,Please enable in Settings", delegate: nil, cancelButtonTitle: "OK")
//      alertView.show()
//    }
//    
//
//  }
  
  func currentViewController() -> UIViewController? {
    guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController else {
      return nil
    }
    if let presentedViewController = rootViewController.presentedViewController{
      return presentedViewController
    }else{
      return rootViewController
    }
  }
  
//  func applicationStatues() -> UIApplicationState {
//    return UIApplication.sharedApplication().applicationState
//  }
  
  var lastBackgroundLocation: ((CLLocation) -> Void)?
  
  private func begionBackgroundTask(time: TimeInterval){
    initialBackgroundTask()
    
    lastBackgroundLocation?(backgroundLocations.last!)
    backgroundLocations.removeAll()
    
    //    self.performSelector(#selector(againStartLocation), withObject: nil, afterDelay: time)
    timer = Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(againStartLocation), userInfo: nil, repeats: false)
    
    backgroundTask.registerBackgroundTask()
    
    print(" stop location :\(time) seconds")
  }
  
  private func initialBackgroundTask() {
    if backgroundTask.tasking {
      backgroundTask.endBackgroundTask()
    }
    if let timer = timer {
      timer.invalidate()
    }
  }
  
  func againStartLocation(){
    
    ZJLocationService.startLocation()
  }

  
}
