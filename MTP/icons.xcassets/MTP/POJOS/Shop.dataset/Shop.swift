//
//  User.swift
//  ShareInApp
//
//  Created by Erick on 04/08/15.
//  Copyright (c) 2015 ShareIn. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class Shop {
    
    var id:String=""
    var name:String=""
    var ret_id:String=""
    var street:String=""
    var number:String=""
    var zip:String=""
    var lat:Double=0.0
    var lon:Double=0.0
    
    init(){
    }

    
    
    init(JSON_User_Info jsonDictionary:JSON){
        self.id=jsonDictionary["_id"].string!
        self.name=jsonDictionary["name"].string!
        self.ret_id=jsonDictionary["ret_id"].string!
        self.street=jsonDictionary["address"]["street"].string!
        self.number=jsonDictionary["address"]["number"].string!
        self.zip = jsonDictionary["address"]["zip"].string!
        self.lat = jsonDictionary["pos"]["lat"].double!
        self.lon = jsonDictionary["pos"]["lon"].double!
    }
}
