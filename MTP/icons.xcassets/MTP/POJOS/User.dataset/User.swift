//
//  User.swift
//  ShareInApp
//
//  Created by Erick on 04/08/15.
//  Copyright (c) 2015 ShareIn. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class User {
    var username:String=""
    var email:String=""
    var password:String=""
    var token:String=""
    var date_birth:String=""
    var id:String=""
    var idPush:String=""
    var aboutMe:String=""
    var origin_place:String=""
    var image:UIImage=UIImage()
    var mutualFriends:Int=0
    var totalTravels:Int=1
    var social_id:String=""
    var years:Int=1
    var year_Join:Int=1
    var month_join:Int=1
    
    init(){
    }
    
    init (username:String,id:String){
        self.id=id
        self.username=username
        // self.car=Car(id: idC, marca: marca, modelo: modelo)
    }
    
    
    init(JSON_User_Info jsonDictionary:JSON){
        self.id=jsonDictionary["id"].string!
        self.username=jsonDictionary["name"].string!
        self.email=jsonDictionary["email"].string!
        self.token=jsonDictionary["token"].string!
        
        if (jsonDictionary["placeOrigin"].null != nil){
            self.origin_place=jsonDictionary["placeOrigin"].string!
        }
        let imageD=jsonDictionary["photo"].string!
        let decodedData = Data(base64Encoded: imageD, options: NSData.Base64DecodingOptions(rawValue: 0))
        let decodedimage = UIImage(data: decodedData!)
        self.image = decodedimage!
    }
}
