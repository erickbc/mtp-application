
//
//  Color.swift
//  MTP
//
//  Created by Erick on 7/11/17.
//  Copyright © 2017 Erick. All rights reserved.
//

import UIKit



extension UIColor{
    class func getCustomColorOrange() -> UIColor{
        
        
        return  UIColor(red: 253/255, green: 110/255, blue: 55/255, alpha: 1)

       // return UIColor(red:0.043, green:0.576 ,blue:0.588 , alpha:1.00)
    }
    
    
    class func getCustomColorLightOrange() -> UIColor{
        
        
        return  UIColor(red: 255/255, green: 152/255, blue: 0/255, alpha: 1)
        
        // return UIColor(red:0.043, green:0.576 ,blue:0.588 , alpha:1.00)
    }
    
    class func getCustomColorLStrongGray() -> UIColor{
        
        
        return  UIColor(red: 132/255, green: 132/255, blue: 132/255, alpha: 1)
        
        // return UIColor(red:0.043, green:0.576 ,blue:0.588 , alpha:1.00)
    }
    
    class func getCustomColorLLightGray() -> UIColor{
        
        
        return  UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        
        // return UIColor(red:0.043, green:0.576 ,blue:0.588 , alpha:1.00)
    }
}
