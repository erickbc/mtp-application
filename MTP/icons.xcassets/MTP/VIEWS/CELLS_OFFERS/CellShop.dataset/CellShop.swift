//
//  HomeBestStyleCommentCell.swift
//  ShareInApp
//
//  Created by Erick on 9/21/15.
//  Copyright (c) 2015 ShareIn. All rights reserved.
//

import UIKit

class CellShop: UITableViewCell {
    
    
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var descriptionShop: UILabel!
    @IBOutlet weak var addressShop: UILabel!
    @IBOutlet weak var linkShopFacebook: UIImageView!
    @IBOutlet weak var imageShop: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.imageShop.layer.cornerRadius = self.imageShop.frame.size.width / 2;
        self.imageShop.layer.borderWidth = 4.0;
        
        let swiftColor = UIColor(red: 22/255, green: 113/255, blue: 197/255, alpha: 1)
        
        
        self.imageShop.layer.borderColor = swiftColor.cgColor
        self.imageShop.clipsToBounds = true
        
        
        //self.backgroundColor = UIColor(patternImage: UIImage(named: "Globe.png")!)
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
