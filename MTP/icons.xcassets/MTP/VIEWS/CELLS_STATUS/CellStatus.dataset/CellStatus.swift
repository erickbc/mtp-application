//
//  HomeBestStyleCommentCell.swift
//  ShareInApp
//
//  Created by Erick on 9/21/15.
//  Copyright (c) 2015 ShareIn. All rights reserved.
//

import UIKit

class CellStatus: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var nameStatus: UILabel!
    @IBOutlet weak var iconStatus: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        print("Width")
        
        print(self.frame.width)
        
        
        // let whiteRoundedCornerView:UIView = UIView(frame: CGRect(x: 5,y: 5,width: 365,height: 135))
        
        viewBackground.backgroundColor = UIColor.white
        viewBackground.layer.masksToBounds = false
        viewBackground.layer.cornerRadius = 3.0;
        viewBackground.layer.shadowOffset = CGSize(width: -1, height: 1);
        viewBackground.layer.shadowOpacity = 0.5
        
        
        self.backgroundColor = UIColor.clear
    
        
        
        
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
