//
//  InfoProduct.swift
//  MaketiOs
//
//  Created by Erick on 1/22/17.
//  Copyright © 2017 Erick. All rights reserved.
//

import UIKit

class InfoProduct:UIViewController{
    
    var info:String = ""
    
    @IBOutlet weak var tilte: UITextField!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var descriptionProduct: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (info == "Liverpool"){
            tilte.text = "Play Station 4"
            descriptionProduct.text = "Buy PS4 console and get prepared for the most immersive gaming experience ever! Enjoy exclusive PlayStation 4 games and exciting PS4 features."
            imageProduct.image = UIImage(named: "playstation_4.jpg")
            
        }else if (info == "Boston"){
            
            tilte.text = "Alitas Extreme"
            descriptionProduct.text = "Aprovecha nuestras promociones en Boston Hot Dogs de Lunes a Domingo ¡También tenemos paquetes y combos ejecutivos! ¿Qué esperas?"
            imageProduct.image = UIImage(named: "alitas_pollo.jpeg")
            
        }else if (info == "Innova"){
            
            
            tilte.text = "Jerseys de Mexico"
            descriptionProduct.text = "Descubre lo mejor en accesorios, ropa y calzado deportivo en Innovasport. Las mejores marcas como Nike, Adidas, Puma y Under Armour viven aquí."
            
            imageProduct.image = UIImage(named: "mexico_jersey.jpg")
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
}
