//
//  TableOffersDetail.swift
//  MTP
//
//  Created by Erick on 7/4/17.
//  Copyright © 2017 Erick. All rights reserved.
//

import UIKit


class TableOffersDetail: UITableViewController{
    
    
    var store:Shop = Shop()
    override func viewDidLoad() {
        print("Chare View Table")
        self.view.backgroundColor = UIColor.getCustomColorLLightGray()
            
            //UIColor(patternImage: UIImage(named: "back_xhdpi_perfil.png")!)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    
        return 20
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
        
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "seeDescription", sender: nil)

    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        var cell:UITableViewCell
        
        
        if (indexPath.row == 0){
            let cellPrincipal = (tableView.dequeueReusableCell(withIdentifier: "CellShop", for: indexPath as IndexPath) ) as! CellShop
            cellPrincipal.selectionStyle = UITableViewCellSelectionStyle.none
            
            cellPrincipal.addressShop.text = "\(store.street) #\(store.number) C.P \(store.zip)"
             cellPrincipal.descriptionShop.text = "Tienda Departamental"
            cellPrincipal.shopName.text = store.name
            
            
            cell = cellPrincipal
            
            return cell
        }else{
            let cellPrincipal = (tableView.dequeueReusableCell(withIdentifier: "CellOffer", for: indexPath as IndexPath) ) as! CellOffer
            
            cellPrincipal.backgroundColor = UIColor.clear
            
            cellPrincipal.descriptionOffer.text = "Shoes Nike"
            cellPrincipal.nameOffer.text = "Nike's Back to the Future Shoes"
            cellPrincipal.priceOffer.text = "$ 3789.00"
           
            cell = cellPrincipal
            
            return cell
        }
    
    }
    

}
