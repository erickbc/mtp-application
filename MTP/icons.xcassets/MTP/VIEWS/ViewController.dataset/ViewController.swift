//
//  ViewController.swift
//  MTP
//
//  Created by Erick on 6/28/17.
//  Copyright © 2017 Erick. All rights reserved.
//

import Alamofire
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


class ViewController: UIViewController,FBSDKLoginButtonDelegate{

    
    @IBOutlet weak var loginFacebook: FBSDKLoginButton!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    var userLogin:User? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
         loginFacebook.readPermissions = ["public_profile", "email", "user_friends"]
        
        loginFacebook.delegate = self
        
        //Charge User
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
            userLogin = appDelegate.userLogin
        

        
        
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        if error == nil{
            
            let tokenFa = FBSDKAccessToken.current().tokenString
            DispatchQueue.main.async {
                Alamofire.request("https://graph.facebook.com/me/picture?type=large&return_ssl_resources=1&access_token="+tokenFa!).response{
                response in
                    let image = UIImage(data: response.data!)
                    self.userLogin?.image = image!
                    self.performSegue(withIdentifier: "showMap", sender: self)
                }
               
            }
            
        }else{
            print("Error")
            print(error)
            
        }

    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Salida")
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

