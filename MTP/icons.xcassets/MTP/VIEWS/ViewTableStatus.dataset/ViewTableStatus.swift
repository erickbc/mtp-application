//
//  ViewTableStatus.swift
//  MTP
//
//  Created by Erick on 7/12/17.
//  Copyright © 2017 Erick. All rights reserved.
//


import UIKit

class ViewTableStatus:UIViewController, UITableViewDelegate, UITableViewDataSource{


    
    
    var status:[Status] = []
    var statusSelect:Status = Status()
    
    override func viewDidLoad() {
        print("Chare View Table")
        self.view.backgroundColor = UIColor.getCustomColorLLightGray()
        
        let s1:Status = Status()
            s1.description="Enamorado"
            s1.image = UIImage(named: "icon4.png")!
        
        let s2:Status = Status()
            s2.description="Adinerado"
            s2.image = UIImage(named: "icon3.png")!
       
        let s3:Status = Status()
            s3.description="Feliz"
            s3.image = UIImage(named: "icon2.png")!
        
        let s4:Status = Status()
            s4.description="Sonriente"
            s4.image = UIImage(named: "icon1.png")!
        
        status.append(s1)
        status.append(s2)
        status.append(s3)
        status.append(s4)
        
        //UIColor(patternImage: UIImage(named: "back_xhdpi_perfil.png")!)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 45
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        statusSelect = status[indexPath.row]
        performSegue(withIdentifier: "returnToHome", sender: statusSelect)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        
        
        
        let cellPrincipal = (tableView.dequeueReusableCell(withIdentifier: "CellStatus", for: indexPath as IndexPath) ) as! CellStatus
        
        cellPrincipal.backgroundColor = UIColor.clear
        
        let s:Status = status[indexPath.row]
        
        cellPrincipal.nameStatus.text = s.description
        cellPrincipal.iconStatus.image = s.image
        
        
        return cellPrincipal
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let mapView = segue.destination as! MapView
        
        let s:Status = sender as! Status
            statusSelect = s
            mapView.status = s
    }
}
