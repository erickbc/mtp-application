//
//  DjangoRequest.swift
//  ShareIn
//
//  Created by Erick on 10/20/16.
//  Copyright © 2016 ShareIn. All rights reserved.
//

import Alamofire
import UIKit

class ServiceWeb {
    
    enum Router: URLRequestConvertible {
        
        
        static let baseURLString = "http://138.68.240.229:8085/"
        
        
        static var tokenUser:String?
        //Cases
        
        //Registrer Facebook
        case getNearStores(String,String)
        case getStoreDetail(String)
        case getProductoOffer(String)
        
        func asURLRequest() throws -> URLRequest {
            
            switch self {
                //*************   Registrer Facebook  ******************
                
                
                
            case .getNearStores(let lat, let long):
                let path: (path: String, parameters: [String:String]) = {
                    
                    var parameters:[String: String]

                    
                    parameters = [
                        "lat": lat,
                        "lon": long,
                    ]
                    
                    return ("mtp/mbl/api/stores", parameters)
                }()
                
                let url = try Router.baseURLString.asURL()
                var urlRequest = URLRequest(url: url.appendingPathComponent(path.path))
                    urlRequest.httpMethod="GET"
                 
                
                return try URLEncoding.default.encode(urlRequest, with: path.parameters)
                
            case .getStoreDetail(let idStore):
                let path: (path: String, parameters: [String:String]) = {
                    return ("mtp/mbl/api/store", ["strId":idStore])
                }()
                
                let url = try Router.baseURLString.asURL()
                var urlRequest = URLRequest(url: url.appendingPathComponent(path.path))
                urlRequest.httpMethod="GET"
                urlRequest = try URLEncoding.default.encode(urlRequest, with: path.parameters)
                
                return urlRequest
                
            case .getProductoOffer(let idOffer):
                let path: (path: String, parameters: [String:String]) = {
                    return ("mtp/mbl/api/productOfferStore", ["pof":idOffer])
                }()
                
                let url = try Router.baseURLString.asURL()
                var urlRequest = URLRequest(url: url.appendingPathComponent(path.path))
                urlRequest.httpMethod="GET"
                urlRequest = try URLEncoding.default.encode(urlRequest, with: path.parameters)
                
                return urlRequest
                
            }
        }
    }
    
}
